# Лабораторная работа 1. Выстрел  в  мишень

### Постановка задания
Математическими объектами очерчивается область (мишень)    
1.	Генерируя случайными числами координаты точки  , подсчитать в процентах количество попаданий в мишень. 
2.	Подсчет закончить, когда встретиться точка   или осуществится ровно   (  – константа) проб. 
3.	Предусмотреть вывод на печать протокола выстрелов.

[Варианты заданий.](Варианты_задания._Выстрел_в_мишень.pdf)
